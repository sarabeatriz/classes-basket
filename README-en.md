
# Classes - Basketball


![main1.png](images/main1.png)
![main2.png](images/main2.png)
![main3.png](images/main3.png)

[Verano 2016 - Ive- Tatiana]

*Object Oriented Programming* (OOP) is a programming paradigm that promotes the design of programs by having different objects interacting together to solve a problem. C++ is one of the programming languages that promotes object oriented programming, allowing programmers to create their own classes from scratch or derive them from other existing classes. Other languages that promote OOP are Java, Python, JavaScript and PHP.

In OOP, each object encapsulates within itself certain properties about the entity being modeled. For example, an object that models a *point* encapsulates the coordinates *x* and *y* of the point being represented. Furthermore, each object allows certain actions to be carried out on itself with the *methods* that the object contains. For example, an object of class *point* could carry out the action of changing the value of the *x* coordinate.

When an object class we need to use in our program has not been predefined in a library, we need to declare and implement our own class. To do this, we define *classes* that contain data with certain *properties* or *attributes*, and actions that we want to carry out with this data through the use of *methods* or *member functions*. This way, we can organize the information and processes in objects that have the properties and methods of a class. In today's laboratory experience, you will practice defining a class and implementing some of its methods by completing a program that simulates a basketball game between two players, maintaining the score for the game and the global statistics for the two players.

## Objectives:

1. Practice the implementation and declaration of classes in C++.

2. Implement methods in a class.


## Pre-Lab:

Before coming to the laboratory you should have:

1. Reviewed the concepts related to classes and objects.

2. Studied the skeleton for the program in `main.cpp`.

3. Studied the concepts and instructions for the laboratory session.

4. Taken the Pre-Lab quiz, available in Moodle.

---

---

##Classes and Objects in C++

An *object* is an entity that contains data and procedures to manipulate them. Similar to how each variable has a *type* of data associated to it, each object has a *class* associated to it, which describes the properties of the the objects: its data (*attributes*), and the procedures that can be used to manipulate its data (*methods*).

It is not necessary to know all of the details about the methods of the object to define and use an object, but you must know how to create it and how to interact with it. The necessary  information is available in the class' documentation. Before creating objects of any class, we should familiarize ourselves with its documentation. The documentation indicates, among other things, what entity is trying to be represented in the class, and its interface or methods available to manipulate the objects of the class.

To see an example of a class, take a look at the documentation of the `Bird` class which can be found in [this link](doc/en/html/index.html).

###Classes

A class is a description of the data and processes of an object. The class’ declaration establishes the attributes that each of the objects of the class will have, and the methods that it can invoke.

If it isn't specified otherwise, the attributes and methods defined in a class will be private. This means that the variables can only be accessed and changed by the methods of the class (*constructors*, *setters*, and *getters*, among others).

The following is the skeleton of the declaration of a class:

---

```
  class ClassName
   {
    // Declarations

    private:
      // Declaration of variables or attributes
      // and prototype member functions
      // that are private for this class

      type privateVar
      type nameOfPrivateMemFunc(type of the parameters);

    public:
      // Declarations of attributes
      // and prototypes of method functions
      // that are public for the entire program

      type publicVar;
      type nameOfPublicMemFunc(type of the parameters);
   };
```

---

---

## The Game

The skeleton for the program that we provide simulates a basketball game between two players. The program uses OOP and simulates the game by creating an object of class BBPlayer for every player and calling its methods. The object that represents every player contains attributes such as the name of the player and the game statistics, i. e. throws taken and made. During the game, a successful throw into the basket receives two points. The program includes random functions and formulas to determine the player that attempts a throw, if the player scores, and the player that takes the rebound. During the simulated game the score for each player is kept and each player's data is updated. At the end of the game, a table with the statistics is displayed.

The algorithm that simulates the game is the following:

```
1. initialize the player
2. assign jugadorEnOfensiva randomly
3. while (none has won)
4.   simulate a shot to the basket
5.   if (scored)
6.     update the statistics of the player that shot the basket
7.     if (the score >= 32):
8.        inform that the offense player (jugadorEnOfensiva) won 
9.     else:
10.       exchange offensive player
11.  else:
12.    update statistics of the player that shot the basket
13.    determine who gets the rebound
14.    update the statistics of the player that got the rebound
15.    assign a player in offense (jugadorEnOfensiva)
16. show the players statistics
```

---

### The `BBPlayer` Class

For this laboratory experience, you will define the `BBPlayer` class that contains the attributes and methods that are described below. Since some of the functions that are already defined in the provided code use attributes and methods of this class, it is important that you use the same names that we indicate. The commented code in the `main` function and in `test_BBPlayer` will help you determine the type of data the method should return, the types the parameters should have, and the order in which they are included in the function declaration.


#### Attributes

* `_name`: stores the name of the player
* `_shotsTaken`: stores the number of attempted throws during the tournament
* `_showsMade`: stores the number of throws scored during the tournament
* `_gamesPlayed`: stores the number of games played during the tournament
* `_rebounds`: stores the number of rebounds during the tournament
* `_score`: stores the player's score during the game


#### Methods

* default constructor. *(method included in `main.cpp`)*
* `setAll()`: method that assigns an initial value to all of the attributes of the object. Notice that this method is invoked at the beginning of `main` to assign initial values to the array `P` that is an array of two objects of the `BBPlayer` class.
* `name()`: method to acquire the name of the player.
* `shotPercentage()`: method to compute the percent of throws scored; is used to determine if the attempted throw is scored by a player. *(method included in `main.cpp`)*
* `reboundsPerGame()`: method to compute the average number of rebounds caught by a player; is used to determine if the player successfully catches the rebound. *(method included in `main.cpp`)*
* `shotMade()`: method that registers that a shot was scored, that a shot was attempted, and updates the score for the player.
* `shotMissed()`: method that registers an attempted throw (but unsuccessful).
* `reboundMade()`: method that registers that a rebound was caught.
* `addGame()`: method that registers that a game was played.
* `score()`: method to acquire the score of a player.
* `printStats()`: method that displays a player's statistics. *(method included in `main.cpp`)*

---

---

!INCLUDE "../../eip-diagnostic/basket/en/diag-basket-01.html"
<br>

!INCLUDE "../../eip-diagnostic/basket/en/diag-basket-02.html"
<br>

---

---

## Laboratory Session:

In this laboratory experience, your task will be to define the `BBPlayer` class with the attributes and method prototypes listed above. The skeleton for the program includes the code to define some of the methods; others you will have to be define.

The skeleton for the program also includes the `test_BBPlayer` function that does unit tests to each of the functions in the program. The tests are commented and, as you define each function, you will remove the comments, test and modify the function, until the test for that function is passed. Once all of the functions are ready and have passed the tests, the whole program is tested by removing the necessary comments in the `main` function.


### Exercise 1 - Download and understand the provided code

#### Instructions

1. Load the project  `basket01` into `QtCreator`. There are two ways to do this:

    * Using the virtual machine: Double click the file `basket01.pro` located in the folder `/home/eip/labs/classes-basket01` of your virtual machine.
    * Downloading the project’s folder from `Bitbucket`: Use a terminal and write the command `git clone http:/bitbucket.org/eip-uprrp/classes-basket01` to download the folder `classes-basket01` from `Bitbucket`. Double click the file `basket01.pro` located in the folder that you downloaded to your computer.

2. This program will run on the terminal and you should select this option from the "Projects" window. To access this window, you select "Projects" in the vertical menu to the left. Afterwards, in `Build & Run` you select `Run` and then mark the box that says `Run in terminal`.

3. Study and understand the code in the `main.cpp` file, including the code that is commented.

### Exercise 2 - Define the `BBPlayer` class

#### Instructions

1. Define the `BBPlayer` class with the specifications included in the section **The BBPlayer Class**. For each one of the methods that are specified above:

    a. Include a prototype for the method in the class definition.

    b. If the corresponding member function is already coded in the program, remove the comments from the corresponding sections for this function in `test_BBPlayer`.

    c. If the corresponding member function is not coded, define the function and then remove the comments from the corresponding sections for this function in `test_BBPlayer`.

    d. Run the program and verify that it passed all of the tests. You should obtain a window similar to the one in Figure 1. If your code does not pass all the tests, revise your code. Repeat until your code has passed all of the tests.

    ---

    ![figure1.png](images/figure1.png)

    **Figure 1.** Example of the window you should obtain if the code passes the unit tests.

    ---

2. Once you have all of the functions defined and tested, uncomment the code in the `main` function to test the whole program. If the program functions correctly, you should obtain a window that starts similarly to the one in Figure 2 and finishes like the one in Figure 3.

    ---


    ![figure2.png](images/figure2.png)


    **Figure 2.** Example of the start of the window you should obtain if the program functions correctly.

    ---


    ![figure3.png](images/figure3.png)


    **Figure 3.** Example of the window you should obtain at the end of the program if it is working correctly.

    ---

    **IMPORTANT:** You SHOULD NOT make any changes in the `main` and `test_BBPlayer` functions, aside from removing the comments.

    ---

    ---

## Deliverables

Use "Deliverable" in Moodle to hand in the `main.cpp` file. Remember to use good programming techniques, include the name of the programmers involved, and document your program.

---

---

## References

[1] http://www.telegraph.co.uk/sport/olympics/basketball/9348826/London-2012-Olympics-Temi-Fagbenle-in-Team-GB-womens-basketball-squad.html

[2] http://www.musthavemenus.com/category/bar-clipart.html

[3] http://basketball.isport.com/basketball-guides/finding-your-niche-in-basketball



