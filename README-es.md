
# Clases - Baloncesto


![main1.png](images/main1.png)
![main2.png](images/main2.png)
![main3.png](images/main3.png)



La *programación orientada a objetos* (object oriented programming-OOP) es un paradigma de programación que promueve el diseño de programas en el que distintos objetos interactúan entre sí para resolver un problema. C++ es uno de los lenguajes de programación que promueve la programación orientada a objetos, permitiendo que los programadores creen sus propias clases desde cero o derivadas de otras clases existentes. Otros lenguajes que promueven OOP son Java, Python, Javascript y PHP.  

En OOP, cada objeto encapsula dentro de él ciertas propiedades sobre el ente que está modelando. Por ejemplo, un objeto que modela un *punto* que encapsula dentro de sí las coordenadas *x* y *y* del punto que representa. Además, cada objeto permite realizar ciertas acciones sobre sí, i.e. contiene sus *métodos*. Por ejemplo, un objeto de la clase *punto* puede realizar la acción de cambiar el valor de su coordenada *x*.

Cuando la clase de objetos que necesitamos utilizar en nuestro programa no ha sido predefinida en el alguna librería, necesitamos declarar e implementar nuestra propia clase. Para esto, definimos *clases* que contengan datos con ciertas *propiedades* o *atributos*, y acciones que queremos hacer con esos datos por medio de *métodos* o *funciones miembro*. De esta manera, podremos organizar la información y los procesos en *objetos* que tienen las propiedades y métodos de una clase. En la experiencia de laboratorio de hoy, practicarás la implementación de una clase y algunos de sus métodos, completando un programa que simula un partido de baloncesto entre dos jugadores, mantiene la puntuación del juego y las estadísticas globales de los dos jugadores.


## Objetivos:

1. Practicar la declaración e implementación de clases en C++.

2. Implementar métodos en una clase.


## Pre-Lab:

Antes de llegar al laboratorio debes haber:

1. Repasado los conceptos relacionados a clases y objetos.

2. Estudiado el esqueleto del programa en `main.cpp`.

3. Estudiado los conceptos e instrucciones para la sesión de laboratorio.

4. Tomado el quiz Pre-Lab, disponible en Moodle.



---

---

## Clases y objetos en C++

Un *objeto* es un ente que contiene datos y procedimientos para manipularlos. Al igual que cada variable tiene un *tipo* de dato asociada a ella, cada objeto tiene una *clase* asociada que describe las propiedades de los objetos:
sus datos (*atributos*), y los procedimientos con los que se pueden manipular los datos (*métodos*).

Para definir y utilizar un objeto no hay que saber todos los detalles de los métodos del objeto pero hay que saber cómo crearlo, y cómo interactuar con él. La  información necesaria está disponible en la documentación de la clase. Antes de crear objetos de cualquier clase debemos familiarizarnos con su documentación. La documentación nos indica, entre otras cosas, que ente se está tratando de representar con la clase, y cuáles son los interfaces o métodos disponibles para manipular los objetos de la clase.

Dale un vistazo a la documentación de la clase `Bird` que se encuentra en [este enlace](doc/es/html/index.html) para que veas un ejemplo de una clase. 

### Clases

Una clase es una descripción de los datos y procesos de un objeto. La declaración de una clase establece los atributos que tendrá cada objeto de esa clase y los métodos que pueden invocar.


Si no se especifica lo contrario, los atributos y métodos definidos en una clase serán privados. Esto quiere decir que esas variables solo se pueden acceder y cambiar por los métodos de la clase (*constructores*, *"setters"* y *"getters"*, entre otros). 

Lo siguiente es el esqueleto de la declaración de una clase:

---

```
  class NombreClase
   {
    // Declaraciones

    private:
      // Declaraciones de variables o atributos y 
      // prototipos de métodos 
      // que sean privados para esta clase

      tipo varPrivada;
      tipo nombreMetodoPrivado(tipo de los parámetros);

    public:
      // Declaraciones de atributos y 
      // prototipos de métodos 
      // que sean públicos para todo el programa

      tipo varPública;
      tipo nombreMetodoPúblico(tipo de los parámetros);
   };
```

---

---

## El juego

El esqueleto del programa que te proveemos simula un partido de baloncesto entre dos jugadores. El programa utiliza OOP y simula el partido creando un objeto de clase BBPlayer para cada jugador e invocando sus métodos. El objeto que representa cada jugador contiene atributos tales como el nombre del jugador y estadísticas de juego, e.g. tiros intentados y anotados. Durante el juego, cada tiro anotado vale dos puntos. El programa incluye funciones  aleatorias y fórmulas para determinar el jugador que intenta el tiro, si anota el tiro y el jugador que coge el rebote.  Durante el partido simulado se mantiene la puntuación de cada jugador y se actualizan los datos para cada jugador. Al final del partido se despliega una tabla con las estadísticas.

El algoritmo que simula el juego es el siguiente:

```
1.  inicializar jugadores
2.  asignar jugadorEnOfensiva aleatoriamente
3.  while (ninguno ha ganado):
4.    simular tiro al canasto
5.    if (anotó):
6.      actualizar estadísticas de quien tiró al canasto
7.      if (puntuación >= 32):
8.        informar que jugadorEnOfensiva ganó
9.      else:
10.        intercambiar jugadorEnOfensiva
11.   else:
12.     actualizar estadísticas de quien tiró al canasto
13.     determinar quién atrapa rebote
14.     actualizar estadísticas de quien atrapó rebote
15.     asignar jugadorEnOfensiva
16. mostrar estadísticas de los jugadores
```

---

### La clase `BBPlayer`

Para esta experiencia de laboratorio, definirás una clase `BBPlayer` que contenga los atributos y métodos que se describen a continuación. Como algunas de las funciones que ya están definidas en el código provisto utilizan atributos y métodos de esta clase, es importante que utilices los mismos nombres que te indicamos. El código comentado en la función `main` y en `test_BBPlayer` también te ayudará a determinar el tipo de dato que debe devolver el método, el orden y el tipo de sus parámetros.

#### Atributos

*  `_name`: guardará el nombre del jugador
*  `_shotsTaken`: guardará el número de tiros al canasto intentados durante el torneo
*  `_shotsMade`: guardará el número de tiros al canasto anotados durante el torneo
*  `_gamesPlayed`: guardará el número de juegos jugados durante el torneo
*  `_rebounds`: guardará el número de rebotes durante el torneo
*  `_score`: guardará la puntuación del jugador durante el partido

#### Métodos

* constructor por defecto (default). *(método incluido en `main.cpp`)*
* `setAll()`: método que asigna valor inicial a todos los atributos de un objeto. Nota que este método está invocado al comienzo de `main` para asignarle valores iniciales al arreglo `P` que es un arreglo de dos objetos de la clase `BBPlayer`.
* `name()`: método para adquirir el nombre del jugador.
* `shotPercentage()`: método para calcular el porciento de tiros anotados; durante el juego se usa para determinar si el tiro intentado por un jugador se anota. *(método incluido en `main.cpp`)*
* `reboundsPerGame()`: método para calcular el promedio de rebotes atrapados por juego; durante el juego se usa para determinar cuál de los jugadores atrapa un rebote. *(método incluido en `main.cpp`)*
* `shotMade()`: método que registra que se anotó un tiro, i.e. incrementa _shotsMade, _shotsTaken y el _score del jugador.
* `shotMissed()`: método que registra tiro fallado, i.e. incrementa solo _shotsTaken del jugador.
* `reboundMade()`: método que registra un rebote atrapado.
* `addGame()`: método que registra que se jugó un partido.
* `score()`: método para adquirir la puntuación del jugador.
* `printStats()`: método para desplegar las estadísticas del jugador. *(método incluido en `main.cpp`)*

---

---




!INCLUDE "../../eip-diagnostic/basket/es/diag-basket-01.html"
<br>

!INCLUDE "../../eip-diagnostic/basket/es/diag-basket-02.html"
<br>

---

---

## Sesión de laboratorio:

Tu tarea durante la sesión de laboratorio será definir la clase `BBPlayer` con los atributos y prototipos de los métodos que se listan arriba. El esqueleto del programa incluye la definición de algunos métodos. Tu debes completar el resto.

El esqueleto del programa también incluye la función `test_BBPlayer` que hace pruebas unitarias a cada una de las funciones del programa. Las pruebas están comentadas y, según vayas definiendo las funciones, debes irlas descomentando, probando y modificando la función hasta que pase la prueba. Una vez todas las funciones estén listas y hayan pasado las pruebas, probarás el programa completo removiendo los comentarios que sean necesarios en el código de la función `main`.


### Ejercicio 1 - Bajar y entender el código provisto

#### Instrucciones

1. Carga a `QtCreator` el proyecto `basket01`. Hay dos maneras de hacer esto:

    * Utilizando la máquina virtual: Haz doble “click” en el archivo `basket01.pro` que se encuentra  en el directorio `/home/eip/labs/classes-basket01` de la máquina virtual.
    * Descargando la carpeta del proyecto de `Bitbucket`: Utiliza un terminal y escribe el comando `git clone http:/bitbucket.org/eip-uprrp/classes-basket01` para descargar la carpeta `classes-basket01` de `Bitbucket`. En esa carpeta, haz doble “click” en el archivo `basket01.pro`.

2. El ejecutable correrá en el terminal y  debes seleccionar esa opción desde la ventana de "Projects". Para acceder a esta ventana, seleccionas "Projects" en el menú vertical de la izquierda. Luego en `Build & Run` seleccionas `Run` y luego marcas la caja que dice `Run in terminal`.

3. Estudia el código en el archivo `main.cpp`, incluyendo el código que está comentado.

### Ejercicio 2 - Definir la clase `BBPlayer`

####Instrucciones

1. Define la clase `BBPlayer` con las especificaciones que se proveen en la sección **La clase BBPlayer**. Para cada uno de los métodos que se especifican en esa sección:

    a. Incluye su prototipo en la definición de la clase.

    b. Si el método ya ha sido implementado, remueve los comentarios de las secciones correspondientes a esa función en  `test_BBPlayer`.

    c. Si el método no ha sido implementado, hazlo y luego remueve los comentarios de las secciones correspondientes a esa función en  `test_BBPlayer`.

    d. Corre el programa y verifica que haya pasado las pruebas. Debes obtener una ventana similar a la de la Figura 1, i.e. debe contener el mensaje `All unit tests passed!!!` . Si no pasa las pruebas, revisa tu código. Repite hasta que tu código haya pasado todas las pruebas.

    ---

    ![figure1.png](images/figure1.png)

    **Figura 1.** Ejemplo de la pantalla que debes obtener si el código pasa las pruebas unitarias.

    ---

2. Una vez tengas todas los métodos definidos y probados, descomenta el código de la función `main` para probar el programa completo. Si el programa funciona correctamente, debes obtener una ventana que comienza de manera similar a la Figura 2 y termina como en la Figura 3.

    ---

    ![figure2.png](images/figure2.png)

    **Figura 2.** Ejemplo del comienzo de la pantalla que debes obtener si el programa funciona correctamente.

    ---

    ![figure3.png](images/figure3.png)

    **Figura 3.** Ejemplo de la pantalla que debes obtener al final del programa si éste funciona correctamente.

    ---

    **IMPORTANTE:** NO debes hacer ningún cambio en las funciones  `main` y `test_BBPlayer`, aparte de quitar los comentarios.

---

---

## Entrega

Utiliza "Entrega" en Moodle para entregar el archivo `main.cpp`. Recuerda utilizar buenas prácticas de programación, incluye el nombre de los programadores y documenta tu programa.



---

---


## Referencias

[1] http://www.telegraph.co.uk/sport/olympics/basketball/9348826/London-2012-Olympics-Temi-Fagbenle-in-Team-GB-womens-basketball-squad.html

[2] http://www.musthavemenus.com/category/bar-clipart.html

[3] http://basketball.isport.com/basketball-guides/finding-your-niche-in-basketball

---

---

---